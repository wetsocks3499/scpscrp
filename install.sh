#!/bin/sh 
INSTALL_DIR="${HOME}/.local/share/scpscrp/"
BUILD_DIR="./target/uberjar/"

function generate_script_string {
    JAR_FILE_PATH="${1}"
    printf '#!/bin/sh\njava -jar "%s" $@ \n' "$JAR_FILE_PATH"
}

printf "[INFO]: Running 'lein uberjar'.\n\n"

lein uberjar

if ! [ $? = 0 ]
then
    printf "\n[DON'T PANIC]: 'lein uberjar' exited with an error.\n"
    exit 1
else
    standalone_jar="$(ls -1 ${BUILD_DIR} | grep standalone)"
    if [ -f ${BUILD_DIR}/${standalone_jar} ]
    then
	printf "\n[INFO]: Standalone .jar file found (${standalone_jar})\n"

	# create a directory and install the jar into it
	if ! [ -d "${INSTALL_DIR}" ]
	then
	    printf "[WARN]: Directory ${INSTALL_DIR} does not exist. Creating it now.\n"
	    mkdir "${INSTALL_DIR}"
	fi
	
	printf "[INFO]: Moving .jar file to ${INSTALL_DIR}\n"
	cp "${BUILD_DIR}/${standalone_jar}" "${INSTALL_DIR}"
	
	# create and install the run script
	printf "[INFO]: Generating run script.\n"

	generate_script_string "${INSTALL_DIR}/${standalone_jar}" >> ./scpscrp
	chmod +x ./scpscrp
	if ! [ -d "${HOME}/.local/bin/" ]
	then
	    printf "[WARN]: Directory ${HOME}/.local/bin/ does not exist. Creating it now.\n"
	    mkdir "${HOME}/.local/bin/"
	fi

	printf "[INFO]: Moving run script to ${HOME}/.local/bin/\n"
	mv ./scpscrp "${HOME}/.local/bin/"
	
	printf "[INFO]: All done! No errors detected.\n"
    else
	printf "[DON'T PANIC]: Can't find the standalone .jar file.\n"
	exit 1
    fi
fi

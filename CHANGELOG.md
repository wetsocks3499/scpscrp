# CHANGELOG
"All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)."

## [unreleased]
### 2024-12-09

## Added
- Finally set up version control.

### 2024-12-10

## Fixed
- Function "set-fetch-info" now returns nil instead of throwing an exception (unwanted behavior).

## Changed
- Major code rewrite, stil doesn't work though. Getting closer!
- Improved changelog formatting.

### 2025-02-07

## Added
- Basic functionality! It can only write one filename at the moment, so it's not ready for actual use.
- A reasonable UI.
	
## Fixed
- Forgot to update the changelog. Updating it now.

## [1.0.0]
### 2025-02-08

## Added
- Full functionality! It works!

## Fixed
- `get-fetch-info` no longer trims leading zeros (e.g. '049' changing to '49').

## Changed
- Program exits on garbage input (e.g. "asjkldnasjd").

## [1.1.0]
### 2025-02-11

## Added
- Build/install script added (`install.sh`) so you can execute scpscrp without `java -jar`

## Fixed
- Image URL info not printing to console.

## Changed
- Removed quotes on output directory warning.
- "All done! No errors detected."

## [1.2.0]
### 2025-02-13

## Added
- Proper argument handler, help (`-h`) and version (`-V`) options.
- Accepts multiple article numbers instead of just one(!).
- "Added in version ..." function metadata.

## [1.3.0]
### 2025-02-25

## Added
- Article range function (`-r`), scrape away!
- Slightly better help text.

[unreleased]: https://codeberg.org/sievertslushie/scpscrp/src/branch/main
[1.0.0]: https://codeberg.org/sievertslushie/scpscrp/src/tag/v1.0.0
[1.1.0]: https://codeberg.org/sievertslushie/scpscrp/src/tag/v1.1.0
[1.2.0]: https://codeberg.org/sievertslushie/scpscrp/src/tag/v1.2.0
[1.3.0]: https://codeberg.org/sievertslushie/scpscrp/src/tag/v1.3.0

(ns scpscrp.core
  (:require [org.httpkit.client :as hk-client]
            [hickory.core :as hickory]
            [hickory.select :as hsel])
  (:gen-class))

(def test-url "https://scp-wiki.wikidot.com/scp-106")

(defn get-url-string
  "FIXME docstring"
  [document index]
  (-> (nth document index)
      (find :attrs)
      (second)
      (find :src)
      (second))) ; the string

(defn get-url-vector
  "Sends an HTTP GET request to 'url' and returns ..."
  [url]
  (let [http-promise (hk-client/get (str url))
        parsed-doc (hickory/parse (str @http-promise))
        selected-doc (-> (hsel/select (hsel/child 
                                      (hsel/tag :img)) (hickory/as-hickory parsed-doc)))]
    ;(println selected-doc)
    (let [bound (count selected-doc)]
        
      (loop [i 9
             url-vector []]
        (if (< i bound)
              (recur (inc i) (conj url-vector (get-url-string selected-doc i)))
              url-vector)))))
  
  ; NEXT filter results to just :src URLs

(defn -main
  "FIXME docstring"
  [& args]
  (println (get-url-vector test-url)))


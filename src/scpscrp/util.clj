(ns scpscrp.util
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:gen-class))


(defn duplicate-opts?
  "Checks a list for duplicate command-line options.
  's' is a short form option (-f)
  'l' is a long form option (--foo)
  'lst' is the list to be checked.
  Returns true if duplicates are detected, returns false otherwise."
  {:added "v1.3.0"}
  [lst s l]
  (let [short-filtered (conj (filter #(= s %) lst) nil)
        long-filtered (conj (filter #(= l %) lst) nil)]
    ; conj to prevent arity error
    (if (or (or (not (apply distinct? short-filtered))
                (not (apply distinct? long-filtered)))
            (and (some #{s} short-filtered)
                 (some #{l} long-filtered)))
      true
      false)))


(defn starts-with-dash?
  "Returns true if 's' starts with '-', returns false otherwise."
  {:added "v1.2.0"}
  [s]
  (if (str/starts-with? s "-")
    true
    false))


(defn int-in-string?
  "Returns true if a long int can be parsed from 's', returns false otherwise."
  {:added "v1.2.0"}
  [s]
  (if (number? (parse-long s))
    true
    false))


(defn get-file-name
  "Splits a string by '/' and returns the last element."
  {:added "v1.0.0"}
  [url]
  (last (str/split url #"/")))


(defn write-file!
  "Writes a byte array to disk at the path 'filename' (a string)."
  {:added "v1.0.0"}
  [byte-array filename]
  (with-open [out (io/output-stream (io/file filename))]
    (.write out byte-array)))

(defn range-padded
  "Generates an inclusive range of numbers as strings with leading 0 padding:
  001 002 ... 049 ... 100 ... 3000
  Returns a list of strings."
  {:added "v1.2.0"}
  [start end]
  (let [start (parse-long start)
        end (+ 1 (parse-long end))]
    (map (fn [x] (case (count (str x))
                   1 (str 0 0 x)
                   2 (str 0 x)
                   (str x))) (range start end))))

(ns scpscrp.core
  (:require [org.httpkit.client :as hk-client]
            [hickory.core :as hickory]
            [hickory.select :as hsel]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [scpscrp.util :as util])
  (:gen-class))


(def version "v1.3.0")


(defn do-info-splash
  "Prints information about the program and exits.
  'which' can be :help :version or :bad-opt
  'bad-opt' is a value that gets put in the 'Invalid option' text."
  {:added "v1.2.0"}
  [which & bad-opt]
  (let [help-text (format
"USAGE: scpscrp [-h|-V|-r n1 n2] n ...\n
\t[n]\t\tAn SCP Wiki page (SCP-[n]) to get images from.\n
\t-r, --range\tTakes 2 arguments, directing to scrape SCP-[n1] through SCP-[n2] (inclusive).\n
\t-V, --version\tPrints the version number (%s).\n
\t-h, --help\tPrints this message." version)
        bad-option-text (format "[DON'T PANIC]: Invalid option '%s'.\nSee '-h' for help." (first bad-opt))]
    (case which
      :help (println help-text)
      :version (println version)
      :bad-opt (println bad-option-text)
      nil))
  (System/exit 0))


(defn get-fetch-info
  "Returns a hash map containing an SCP Wiki URL for SCP-n and the string './n/'."
  {:added "v1.0.0"}
  [n]
  (if (util/int-in-string? n)
    (let [info-hash (hash-map :url (format "https://scp-wiki.wikidot.com/scp-%s" n)
                              :out-dir (format "./%s/" n))]
      info-hash)
    (do
      (println (format "[DON'T PANIC]: Invalid input: '%s'." n))
      (System/exit 1))))


(defn get-url-string
  "Finds source URLs in a hickory-processed HTML document starting from index 'index'."
  {:added "v1.0.0"}
  [document index]
  (let [string 
    (-> (nth document index)
        (find :attrs)
        (second)
        (find :src)
        (second))]
    (str/replace string #"\\\"" ""))) ; the string


(defn get-url-vector
  "Sends an HTTP GET request to 'url' and returns a vector of image source URLS."
  {:added "v1.0.0"}
  [url]
  (let [http-promise (hk-client/get (str url))
        parsed-doc (hickory/parse (str @http-promise))
        selected-doc (-> (hsel/select (hsel/child 
                                      (hsel/tag :img)) (hickory/as-hickory parsed-doc)))
        bound (count selected-doc)]
    (loop [i 9
           url-vector []]
      (if (< i bound)
        (recur (inc i) (conj url-vector (get-url-string selected-doc i)))
        url-vector))))


(defn download-image-byte-array
  "Uses http-kit to download the data from 'url' as a byte array."
  {:added "v1.0.0"}
  [url]
  (print (format "[INFO]: Image URL:\t%s -- " url))
  (flush) ; won't show up on terminal otherwise

  (let [promise (hk-client/get url {:as :byte-array}
    (fn [{:keys [status body]}]
      (if (= status 200)
        body
        (println (format "[DON'T PANIC]: URL '%s' returned HTTP status '%s'" url status)))))]

    (println "OK")
    (when promise
      (deref promise))))


(defn get-range-from-cli
  {:added "v1.3.0"}
  [cli-args]
  "Filters command-line arguments to get a 'start' and 'end' for 'util/range-padded',
  then returns the output."
  (let [refilter-args (filter #(or (= "-r" %)
                                   (= "--range" %)
                                   (util/int-in-string? %)) cli-args)
        start-end (->>
                   (drop-while util/int-in-string? refilter-args)
                   (take 3)
                   (drop 1))]
       (util/range-padded (first start-end) (last start-end))))


(defn argument-handler
  "Directs program flow when detecting the following strings:
  (-h --help) (-V --version) (-r --range)
  By default, filters 'args' and returns a list of string-wrapped integers.
  Exits with code 1 if no valid arguments are detected." 
  {:added "v1.0.0"}
  [args]
  (if (not (= args '(nil)))
    (let [options (filter util/starts-with-dash? args)
          numbers (filter util/int-in-string? args)]
      (if (and (empty? options)
               (empty? numbers))
        (do
          (println "[DON'T PANIC]: Invalid input.")
          (System/exit 1)))

      ;; process options
      (doseq [o options] ; do-and-die options
        (case o
          ("-h" "--help") (do-info-splash :help)
          ("-V" "--version") (do-info-splash :version)
          ("-r" "--range") nil
          (do-info-splash :bad-opt o)))
      (if (some #{"-r" "--range"} args)
        (if (not (util/duplicate-opts? options "-r" "--range"))
        (get-range-from-cli args)
        (do
          (println "[DON'T PANIC]: Option '-r' cannot be used multiple times.")
          (System/exit 1)))
        numbers))

    (do
      (println "[DON'T PANIC]: No arguments provided.")
      (System/exit 1))))


(defn -main
  "Main function. First element of 'args' is passed to argument-handler."
  {:added "v1.0.0"}
  [& args]

  (let [number-list (argument-handler args)]

  (doseq [n number-list]
    (let [fetch-info (get-fetch-info n)
          url-vector  (get-url-vector (get fetch-info :url))
          out-dir (get fetch-info :out-dir)]

      (if (not-empty url-vector)
        (do
          ;; first, make the output directory if it doesn't exist
          (when (not (and (.exists (io/file out-dir))
                        (.isDirectory (io/file out-dir))))
            (do
              (println
               (format "[WARN]: Output directory %s does not exist. Creating it now." out-dir))
              (.mkdir (java.io.File. out-dir))))

          ;; download images from 'final-urls' as byte arrays, store them in a lazy seq
          ;; store filenames for the images in another lazy seq,
          ;; then zip both sequences into a hash map.
          (println (format "[INFO]: Wiki URL:\t%s" (get fetch-info :url)))
          (let [final-urls (doall (remove (fn [x] (re-find #"avatar\.php" x)) url-vector)) ; trim
                filenames (for [u final-urls] (util/get-file-name u))
                byte-array-seq (for [u final-urls] (download-image-byte-array u))
                data-map (seq (zipmap byte-array-seq filenames))]

            (doseq [kv data-map]
              (util/write-file! (nth kv 0) (str out-dir (nth kv 1))))))
                                        ; kv nth 0  -- byte array
                                        ; kv nth 1  -- filename string
        
        (println (format "[WARN]: No images detected in article '%s'." (get fetch-info :url))))))

  (println "[INFO]: All done! No errors detected.")))

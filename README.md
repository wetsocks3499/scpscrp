# scpscrp

Image scraper for the SCP Wiki.

## Installation

Run `install.sh`, this will put a standalone .jar file in `~/.local/share/scpscrp/`
and an executable shell script into `~/.local/bin/`

## Usage

	$ scpscrp [args]
	OR
	$ java -jar scpscrp-[VERSION]-standalone.jar [args]

## Options
NOTE: Options are always evaluated first.

	-r, --range    Takes 2 arguments, directing to scrape SCP-[n1] through SCP-[n2] (inclusive).
	-h, --help     Prints the help message.
	-V, --version  Prints the program version.


## Examples

	$ scpscrp 2521
	$ scpscrp 106 682
	$ scpscrp 106 -h      # Prints the help message
	$ scpscrp -V -h       # Prints the version number 
	$ scpscrp -r 106 109  # Same as "106 107 108 109"

### Bugs

Unreliable on newer-styled articles.

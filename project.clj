(defproject scpscrp "1.3.0"
  :description "Image scraper for the SCP Wiki."
  :url "https://codeberg.org/sievertslushie/scpscrp"
  :license {:name "WTFPL"
            :url "http://www.wtfpl.net/"}
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [http-kit/http-kit "2.8.0"]
                 [org.clj-commons/hickory "0.7.4"]]
  :main ^:skip-aot scpscrp.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}})

